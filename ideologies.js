ideologies = [
    {
        "name": "Anarchokomunismus",
        "stats": {
            "econ": 100,
            "dipl": 50,
            "govt": 100,
            "scty": 90
        }
    },
    {
        "name": "Liberální komunismus",
        "stats": {
            "econ": 100,
            "dipl": 70,
            "govt": 80,
            "scty": 80
        }
    },
    {
        "name": "Trockismus",
        "stats": {
            "econ": 100,
            "dipl": 100,
            "govt": 60,
            "scty": 80
        }
    },
    {
        "name": "Marxismus",
        "stats": {
            "econ": 100,
            "dipl": 70,
            "govt": 40,
            "scty": 80
        }
    },
    {
        "name": "De leonismus",
        "stats": {
            "econ": 100,
            "dipl": 30,
            "govt": 30,
            "scty": 80
        }
    },
    {
        "name": "Leninismus",
        "stats": {
            "econ": 100,
            "dipl": 40,
            "govt": 20,
            "scty": 70
        }
    },
    {
        "name": "Stalinismus/Maoismus",
        "stats": {
            "econ": 100,
            "dipl": 20,
            "govt": 0,
            "scty": 60
        }
    },
    {
        "name": "Náboženský komunismus",
        "stats": {
            "econ": 100,
            "dipl": 50,
            "govt": 30,
            "scty": 30
        }
    },
    {
        "name": "Státní socialismus",
        "stats": {
            "econ": 80,
            "dipl": 30,
            "govt": 30,
            "scty": 70
        }
    },
    {
        "name": "Teokratický socialismus",
        "stats": {
            "econ": 80,
            "dipl": 50,
            "govt": 30,
            "scty": 20
        }
    },
    {
        "name": "Náboženský socialismus",
        "stats": {
            "econ": 80,
            "dipl": 50,
            "govt": 70,
            "scty": 20
        }
    },
    {
        "name": "Demokratický socialismus",
        "stats": {
            "econ": 80,
            "dipl": 50,
            "govt": 50,
            "scty": 80
        }
    },
    {
        "name": "Revoluční socialismus",
        "stats": {
            "econ": 80,
            "dipl": 20,
            "govt": 50,
            "scty": 70
        }
    },
    {
        "name": "Liberální socialismus",
        "stats": {
            "econ": 80,
            "dipl": 80,
            "govt": 80,
            "scty": 80
        }
    },
    {
        "name": "Anarchosyndikalismus",
        "stats": {
            "econ": 80,
            "dipl": 50,
            "govt": 100,
            "scty": 80
        }
    },
    {
        "name": "Levicový populismus",
        "stats": {
            "econ": 60,
            "dipl": 40,
            "govt": 30,
            "scty": 70
        }
    },
    {
        "name": "Teokratický distributismus",
        "stats": {
            "econ": 60,
            "dipl": 40,
            "govt": 30,
            "scty": 20
        }
    },
    {
        "name": "Distributismus",
        "stats": {
            "econ": 60,
            "dipl": 50,
            "govt": 50,
            "scty": 20
        }
    },
    {
        "name": "Sociální liberalismus",
        "stats": {
            "econ": 60,
            "dipl": 60,
            "govt": 60,
            "scty": 80
        }
    },
    {
        "name": "Křesťanská demokracie",
        "stats": {
            "econ": 60,
            "dipl": 60,
            "govt": 40,
            "scty": 30
        }
    },
    {
        "name": "Sociální demokracie",
        "stats": {
            "econ": 60,
            "dipl": 70,
            "govt": 40,
            "scty": 80
        }
    },
    {
        "name": "Progresivismus",
        "stats": {
            "econ": 60,
            "dipl": 80,
            "govt": 60,
            "scty": 100
        }
    },
    {
        "name": "Anarchomutualismus",
        "stats": {
            "econ": 60,
            "dipl": 50,
            "govt": 100,
            "scty": 70
        }
    },
    {
        "name": "Národní totalitarismus",
        "stats": {
            "econ": 50,
            "dipl": 20,
            "govt": 0,
            "scty": 50
        }
    },
    {
        "name": "Globální totalitarismus",
        "stats": {
            "econ": 50,
            "dipl": 80,
            "govt": 0,
            "scty": 50
        }
    },
    {
        "name": "Technokracie",
        "stats": {
            "econ": 60,
            "dipl": 60,
            "govt": 20,
            "scty": 70
        }
    },
    {
        "name": "Centrismus",
        "stats": {
            "econ": 50,
            "dipl": 50,
            "govt": 50,
            "scty": 50
        }
    },
    {
        "name": "Liberalismus",
        "stats": {
            "econ": 50,
            "dipl": 60,
            "govt": 60,
            "scty": 60
        }
    },
    {
        "name": "Náboženský anarchismus",
        "stats": {
            "econ": 50,
            "dipl": 50,
            "govt": 0,
            "scty": 20
        }
    },
    {
        "name": "Pravicový populismus",
        "stats": {
            "econ": 40,
            "dipl": 30,
            "govt": 30,
            "scty": 30
        }
    },
    {
        "name": "Umírněný konzervatismus",
        "stats": {
            "econ": 40,
            "dipl": 40,
            "govt": 50,
            "scty": 30
        }
    },
    {
        "name": "Reakcionářství",
        "stats": {
            "econ": 40,
            "dipl": 40,
            "govt": 40,
            "scty": 10
        }
    },
    {
        "name": "Sociální libertarianismus",
        "stats": {
            "econ": 60,
            "dipl": 70,
            "govt": 80,
            "scty": 70
        }
    },
    {
        "name": "Libertarianismus",
        "stats": {
            "econ": 40,
            "dipl": 60,
            "govt": 80,
            "scty": 60
        }
    },
    {
        "name": "Anarchoindividualismus",
        "stats": {
            "econ": 40,
            "dipl": 50,
            "govt": 100,
            "scty": 50
        }
    },
    {
        "name": "Nacismus",
        "stats": {
            "econ": 40,
            "dipl": 0,
            "govt": 0,
            "scty": 5
        }
    },
    {
        "name": "Autokracie",
        "stats": {
            "econ": 50,
            "dipl": 20,
            "govt": 20,
            "scty": 50
        }
    },
    {
        "name": "Fašismus",
        "stats": {
            "econ": 40,
            "dipl": 20,
            "govt": 20,
            "scty": 20
        }
    },
    {
        "name": "Kapitalistický fašismus",
        "stats": {
            "econ": 20,
            "dipl": 20,
            "govt": 20,
            "scty": 20
        }
    },
    {
        "name": "Konzervatismus",
        "stats": {
            "econ": 30,
            "dipl": 40,
            "govt": 40,
            "scty": 20
        }
    },
    {
        "name": "Neoliberalismus",
        "stats": {
            "econ": 30,
            "dipl": 30,
            "govt": 50,
            "scty": 60
        }
    },
    {
        "name": "Klasický liberalismus",
        "stats": {
            "econ": 30,
            "dipl": 60,
            "govt": 60,
            "scty": 80
        }
    },
    {
        "name": "Autoritářský kapitalismus",
        "stats": {
            "econ": 20,
            "dipl": 30,
            "govt": 20,
            "scty": 40
        }
    },
    {
        "name": "Státní kapitalismus",
        "stats": {
            "econ": 20,
            "dipl": 50,
            "govt": 30,
            "scty": 50
        }
    },
    {
        "name": "Neokonzervatismus",
        "stats": {
            "econ": 20,
            "dipl": 20,
            "govt": 40,
            "scty": 20
        }
    },
    {
        "name": "Fundamentalismus",
        "stats": {
            "econ": 20,
            "dipl": 30,
            "govt": 30,
            "scty": 5
        }
    },
    {
        "name": "Libertariánský kapitalismus",
        "stats": {
            "econ": 20,
            "dipl": 50,
            "govt": 80,
            "scty": 60
        }
    },
    {
        "name": "Tržní anarchismus",
        "stats": {
            "econ": 20,
            "dipl": 50,
            "govt": 100,
            "scty": 50
        }
    },
    {
        "name": "Totalitární kapitalismus",
        "stats": {
            "econ": 0,
            "dipl": 30,
            "govt": 0,
            "scty": 50
        }
    },
    {
        "name": "Minarchismus",
        "stats": {
            "econ": 0,
            "dipl": 40,
            "govt": 50,
            "scty": 50
        }
    },
    {
        "name": "Anarchokapitalismus",
        "stats": {
            "econ": 0,
            "dipl": 50,
            "govt": 100,
            "scty": 50
        }
    }
];
