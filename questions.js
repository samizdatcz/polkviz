questions = [
    {
        "question": "Velké firmy představují větší nebezpečí než vlády.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Vládní zásahy do ekonomiky za účelem ochrany spotřebitele jsou nezbytné.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Čím svobodnější trh, tím svobodnější lidé.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Udržovat vyrovnaný rozpočet je důležitější než zajišťovat sociální jistoty pro všechny občany.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Výzkum je přínosnější, když ho financuje stát, než když se řídí nabídkou a poptávkou.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Mezinárodní obchod je prospěšný.",
        "effect": {
            "econ": -5,
            "dipl": 0,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Každý podle svých schopností, každému podle jeho potřeb.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 5
        }
    },
    {
        "question": "Nejlepší by bylo státní sociální programy nahradit individuální dobročinnosti.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": -5
        }
    },
    {
        "question": "Bohatí by měli platit vyšší daně, aby podpořili chudé.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Dědictví je legitimní formou nabytí bohatství.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": -5
        }
    },
    {
        "question": "Infrastruktura jako silniční nebo elektrická síť by měla být ve veřejném vlastnictví.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Nadměrné vládní intervence ohrožují ekonomiku.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Ti, kteří si to mohou dovolit, by měli mít přístup k lepší zdravotní péči.",
        "effect": {
            "econ": -10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Každý člověk má nárok na kvalitní vzdělání.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Výrobní prostředky by měly patřit pracujícím.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Organizace spojených národů by měla být zrušena.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -5,
            "scty": 0
        }
    },
    {
        "question": "Vojenské intervence jsou k ochraně státu nezbytné.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Podporuji Evropskou unii a další regionální uskupení.",
        "effect": {
            "econ": -5,
            "dipl": 10,
            "govt": 10,
            "scty": 5
        }
    },
    {
        "question": "Je důležité uchovat si národní suverenitu.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -5,
            "scty": 0
        }
    },
    {
        "question": "Jednotná světová vláda by lidstvu prospěla.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Udržovat dobré vztahy s ostatními státy je důležitější než rozšiřovat vlastní vliv.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Důvody války není třeba vysvětlovat ostatním zemím.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Náklady na zbrojení jsou plýtváním penězi.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Mezinárodní pomoc je plýtváním penězi.",
        "effect": {
            "econ": -5,
            "dipl": -10,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Můj národ je skvělý.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Vědecký výzkum by se měl provádět na mezinárodní úrovni.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Vlády by se měly zodpovídat mezinárodnímu společenství.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 5,
            "scty": 0
        }
    },
    {
        "question": "Jakékoliv násilí je nepřijatelné – i při protestech proti autoritářskému režimu.",
        "effect": {
            "econ": 0,
            "dipl": 5,
            "govt": -5,
            "scty": 0
        }
    },
    {
        "question": "Moje vlastní náboženské přesvědčení by se mělo co nejvíc rozšířit.",
        "effect": {
            "econ": 0,
            "dipl": -5,
            "govt": -10,
            "scty": -10
        }
    },
    {
        "question": "Naše národní hodnoty by se měly co nejvíc rozšířit.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Je velmi důležité zachovat zákon a pořádek.",
        "effect": {
            "econ": 0,
            "dipl": -5,
            "govt": -10,
            "scty": -5
        }
    },
    {
        "question": "Většinová populace dělá špatná rozhodnutí.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Zločiny bez oběti - například užívání drog, prostituce nebo popírání genocidy - by vůbec neměly být trestné.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "V zájmu boje proti terorismu je nutné obětovat některá naše občanská práva.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Bez vládního dohledu – například monitoringu elektronické komunikace – se dnes neobejdeme.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Hrozbou pro naši svobodu je už samotná existence státní moci.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Nehledě na mé vlastní politické názory je důležité, abych stál za svou zemí.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -10,
            "scty": -5
        }
    },
    {
        "question": "Každá autorita by se měla zpochybňovat.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 5
        }
    },
    {
        "question": "Hierarchicky uspořádaný stát je nejlepší možný.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Je důležité, aby se vláda řídila názorem většiny – i v případě, že je nesprávný.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Čím silnější vedení, tím lépe.",
        "effect": {
            "econ": 0,
            "dipl": -10,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Demokracie je vyšší hodnota, ne jen rozhodovací proces.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Regulace v zájmu ochrany přírody jsou nezbytné.",
        "effect": {
            "econ": 5,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Věda, technologie a automatizace budují lepší svět.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Vzdělávání dětí by mělo vycházet z tradičních hodnot.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": -10
        }
    },
    {
        "question": "Tradice samy o sobě žádnou hodnotu nemají.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Náboženství by mělo hrát roli ve správě státu.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": -10
        }
    },
    {
        "question": "Církve by měly odvádět daně stejně jako jakékoliv jiné organizace.",
        "effect": {
            "econ": 5,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Změna klimatu je jedno z největších ohrožení našeho současného způsobu života.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Je důležité, abychom s klimatickými změnami bojovali sjednoceně jako celý svět.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Naše společnost fungovala v minulosti lépe než dnes.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": -10
        }
    },
    {
        "question": "Je důležité zachovávat tradice minulých generací.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": -10
        }
    },
    {
        "question": "Je důležité uvažovat dlouhodobě, v horizontu delším, než je jeden lidský život.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Racionální rozhodování je důležitější než ochrana naší kultury.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Užívání drog by se mělo legalizovat nebo dekriminalizovat.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 2
        }
    },
    {
        "question": "Stejnopohlavní sňatky by měly být legální.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 10,
            "scty": 10
        }
    },
    {
        "question": "Žádná kultura není nadřazená jiné.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 5,
            "scty": 10
        }
    },
    {
        "question": "Předmanželský sex je nemorální.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -5,
            "scty": -10
        }
    },
    {
        "question": "Pokud máme vůbec přijmout jakékoliv migranty, musí se začlenit do naší kultury.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -5,
            "scty": -10
        }
    },
    {
        "question": "Potrat by měl být ve většině nebo ve všech případech zakázán.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": -10
        }
    },
    {
        "question": "Vlastnictví zbraní by mělo být zakázáno všem kromě těch, kteří k němu mají jasný důvod.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": -10,
            "scty": 0
        }
    },
    {
        "question": "Podporuji univerzální zdravotní péči hrazenou z pravidelných odvodů státu.",
        "effect": {
            "econ": 10,
            "dipl": 0,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Prostituce by měla být nelegální.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": -10,
            "scty": -10
        }
    },
    {
        "question": "Je zásadní zachovat tradiční rodinné hodnoty.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": -10
        }
    },
    {
        "question": "Pokrok za každou cenu je nebezpečný.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": -10
        }
    },
    {
        "question": "Genetické inženýrství je přínosné – i pokud se týká lidí.",
        "effect": {
            "econ": 0,
            "dipl": 0,
            "govt": 0,
            "scty": 10
        }
    },
    {
        "question": "Měli bychom otevřít naše hranice migrantům.",
        "effect": {
            "econ": 0,
            "dipl": 10,
            "govt": 10,
            "scty": 0
        }
    },
    {
        "question": "Vlády by se měly zabývat cizinci stejně jako vlastními občany.",
        "effect": {
            "econ": 10,
            "dipl": 10,
            "govt": 0,
            "scty": 0
        }
    },
    {
        "question": "Se všemi lidmi – bez ohledu na faktory jako kultura či sexualita – by mělo být zacházeno stejně.",
        "effect": {
            "econ": 10,
            "dipl": 10,
            "govt": 10,
            "scty": 10
        }
    },
    {
        "question": "Názory, hodnoty a cíle, k nimž se hlásím já, by se měly prosadit proti těm, které zastávají jiné skupiny občanů.",
        "effect": {
            "econ": -10,
            "dipl": -10,
            "govt": -10,
            "scty": -10
        }
    }
];
